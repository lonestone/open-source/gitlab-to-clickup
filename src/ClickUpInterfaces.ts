export enum Status {
  Open = "Open",
  InProgress = "in progress",
  InReview = "in review",
  Blocked = "blocked",
  Closed = "Closed"
}

export enum Priorities {
  Urgent = "1",
  High = "2",
  Normal = "3",
  Low = "4"
}

export interface IPriority {
  id: Priorities;
  priority: string;
  color: string;
  orderindex: string;
}

export interface IUser {
  id: number;
  username: string;
  color: string;
  profilePicture: string;
  initials: string;
  email: string;
}

export interface ITag {
  name: string;
  tag_fg: string;
  tag_bg: string;
}

export interface IStatus {
  status: Status;
  color: string;
  type: "open" | "blocked" | "custom";
  orderindex: number;
}

export interface ITaskCreation {
  name: string;
  content?: string;
  assignees?: string[]; // user ids
  status?: Status;
  priority?: Priorities;
  due_date?: number;
  tags?: ITag[];
  subtasks?: ITaskCreation[];
}

export interface ITask {
  id: string;
  name: string;
  text_content: string | null;
  status: IStatus;
  orderindex: string;
  date_created: number;
  date_updated: number;
  date_closed: number | null;
  creator: IUser;
  assignees: IUser[];
  tags: ITag[];
  parent: string | null; // id
  priority: IPriority | null;
  due_date: number | null;
  start_date: number | null;
  time_estimate: number | null;
  list: { id: string };
  project: { id: string };
  space: { id: string };
  url: string;
}

export interface ITeam {
  id: string;
  name: string;
  color: string;
  avatar: string;
  members: Array<{
    user: IUser;
  }>;
}

export interface ISpace {
  id: string;
  name: string;
  private: boolean;
  statuses: IStatus[];
  multiple_assignees: boolean;
  // TODO: Features
}

export interface IProject {
  id: string;
  name: string;
  override_statuses: boolean;
  statuses: IStatus[];
  lists: IList[];
}

export interface IListCreation {
  name: string;
}

export interface IList {
  id: string;
  name: string;
}

export interface IComment {
  attachment: [];
  comment: [
    {
      text: string;
    }
  ];
}
